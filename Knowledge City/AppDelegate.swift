//
//  AppDelegate.swift
//  Knowledge City
//
//  Created by Latitude on 09/10/19.
//  Copyright © 2019 Latitude. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        // check user login or not
        if K_UserDefaults.GetToken().count != 0 {
            //if login
            StudentListRootView()
        }else {
            // not login
            LoginVCRootView()
        }
        // setup navgation bar
        UINavigationBar.appearance().barTintColor = AppColor.LiteGrayColor
        UINavigationBar.appearance().tintColor = .white
        UINavigationBar.appearance().isTranslucent = false
        UINavigationBar.appearance().shadowImage = UIImage()
        UINavigationBar.appearance().setBackgroundImage(UIImage(), for: .default)
        
        return true
    }
    //MARK:  Root View login
    func LoginVCRootView(){
        let loginvc = LoginVC.init(nibName: Nibs.LoginVC, bundle: nil)
        let nav = UINavigationController.init(rootViewController: loginvc)
        nav.interactivePopGestureRecognizer?.isEnabled = false
        nav.isNavigationBarHidden = true
        window = UIWindow(frame: UIScreen.main.bounds)
        window!.rootViewController = nav
        window!.makeKeyAndVisible()
    }
    //MARK: Root View studentlist
    func StudentListRootView(){
        let vc = StudentsListVC.init(nibName: Nibs.StudentsListVC, bundle: nil)
        let nav = UINavigationController.init(rootViewController: vc)
        nav.interactivePopGestureRecognizer?.isEnabled = false
        nav.isNavigationBarHidden = false
        window = UIWindow(frame: UIScreen.main.bounds)
        window!.rootViewController = nav
        window!.makeKeyAndVisible()
    }
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

