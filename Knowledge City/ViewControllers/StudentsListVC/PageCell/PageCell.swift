//
//  PageCell.swift
//  Knowledge City
//
//  Created by Latitude on 09/10/19.
//  Copyright © 2019 Latitude. All rights reserved.
//

import UIKit

class PageCell: UICollectionViewCell {

    @IBOutlet weak var lbl_BottomBoarder: UILabel!
    @IBOutlet weak var lbl_PageNumber: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
