//
//  ModelStudentList.swift
//  Knowledge City
//
//  Created by Latitude on 09/10/19.
//  Copyright © 2019 Latitude. All rights reserved.
//

import Foundation

class ModelStudentList
{
    var username = String()
    var first_name = String()
    var last_name = String()
    var group = String()
    
    
    // set data in model method init
    init?(data:[String : AnyObject]?) {
        
        // check data is avialible
        if let ModelData = data{
           
            self.username = ModelData["username"] as? String ?? ""
            self.first_name = ModelData["first_name"] as? String ?? ""
            self.last_name = ModelData["last_name"] as? String ?? ""
            self.group = ModelData["group"] as? String ?? ""
            
        }
        else
        {
            return nil
        }
        
    }
}
