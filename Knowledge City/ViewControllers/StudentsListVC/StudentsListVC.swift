//
//  StudentsListVC.swift
//  Knowledge City
//
//  Created by Latitude on 09/10/19.
//  Copyright © 2019 Latitude. All rights reserved.
//

import UIKit

class StudentsListVC: UIViewController {

    @IBOutlet weak var collecationViewWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var collecationView_Paging: UICollectionView!
    @IBOutlet weak var tbl_StudentList: UITableView!
    var selectedPage = 1
    var totalPage = 0
    var arrStudentList = [ModelStudentList]()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = "User List"
        self.navigationController?.navigationItem.hidesBackButton = true
        collecationView_Paging.register(UINib(nibName:CellNibs.PageCell, bundle: nil), forCellWithReuseIdentifier: CellNibs.PageCell)
        
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        ApiGetStudentList(page: "1")
    }
    
    // log out button click event
    @IBAction func onTapLogoutBTN(_ sender: Any) {
        ApiLogout()
    }
    
    //Api Get student list
    func ApiGetStudentList(page: String) {
        CommonFunctions.shared.ShowHUD()
        let param = ["_start": page,
                     "_limit": "5",
                     "token": K_UserDefaults.GetToken()] as [String:AnyObject]
        
        AFMethods.requestGETURL(Api_StudentList + "\(K_UserDefaults.GetAccount_id())/students", params: param, headers: nil, success: { (response) in
            print(response)
            CommonFunctions.shared.HideHUD()
            let ResponseData = ApiResponseStruct.init(json: response)
            if ResponseData.code == 200 {
                
                let paging = ResponseData.Pagination
                self.totalPage = paging["totalPages"] as! Int + 1
                //adding student data in array
                let arr = ResponseData.dataArray
                self.arrStudentList.removeAll()
                for dic in arr {
                    self.arrStudentList.append(ModelStudentList.init(data: dic)!)
                }
                self.tbl_StudentList.reloadData()
                //reload paging hilight page number
                self.collecationView_Paging.reloadData()
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.0) { // Change `2.0` to the desired number of seconds.
                    self.collecationViewWidthConstraint.constant = CGFloat(self.getCollecationViewContentsize())
                }
                
            } else {
                CommonFunctions.shared.showAlertMessage(vc: self, titleStr: "", messageStr: "Something went wrong.")
            }
        }) { (error) in
            CommonFunctions.shared.HideHUD()
            print(error)
            CommonFunctions.shared.showAlertMessage(vc: self, titleStr: "", messageStr: error.localizedDescription)
        }
    }
    //Logut API
    func ApiLogout() {
        CommonFunctions.shared.ShowHUD()
        let param = ["token": K_UserDefaults.GetToken()] as [String:AnyObject]
        
        AFMethods.requestDeleteURL(Api_Login, params: param, headers: nil, success: { (response) in
            print(response)
            CommonFunctions.shared.HideHUD()
            let respnsedata = ApiResponseStruct.init(json: response)
            
            if respnsedata.code == 200 {
                K_UserDefaults.SaveAccount_id(type: "")
                K_UserDefaults.SaveToken(type: "")
                ApplicationDelegate.LoginVCRootView()
            } else {
                CommonFunctions.shared.showAlertMessage(vc: self, titleStr: "", messageStr: "Something went wrong.")
            }
           
        }) { (error) in
            CommonFunctions.shared.HideHUD()
            print(error)
            CommonFunctions.shared.showAlertMessage(vc: self, titleStr: "", messageStr: error.localizedDescription)
        }
    }
}

//MARK: table view delage and datasource methods
extension StudentsListVC: UITableViewDelegate,UITableViewDataSource {
  
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrStudentList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell : StudentListCell? = tableView.dequeueReusableCell(withIdentifier: CellNibs.StudentListCell) as? StudentListCell
        
        if cell == nil {
            cell = Bundle.main.loadNibNamed(CellNibs.StudentListCell, owner: self, options: nil)?.first as? StudentListCell
        }
        cell?.selectionStyle = .none
        
        cell?.lbl_UserName.text = arrStudentList[indexPath.row].username
        cell?.lbl_FullName.text = "\(arrStudentList[indexPath.row].first_name) \(arrStudentList[indexPath.row].last_name)"
        cell?.lbl_GroupName.text = arrStudentList[indexPath.row].group
        
        if indexPath.row%2 == 0 {
            cell?.contentView.backgroundColor = UIColor.init(hax: 0xECECEC, alpah: 1)
        }else {
            cell?.contentView.backgroundColor = .white
            
        }
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
       
        
    }
}
//MARK: collecation view delage and datasource methods for Paging
extension StudentsListVC: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    // get collecaton view content size
    func getCollecationViewContentsize() -> Float {
        return Float(collecationView_Paging.contentSize.width)
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if selectedPage >= 2 {
            return totalPage + 1
        }
        return totalPage
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CellNibs.PageCell, for: indexPath) as! PageCell
        
        if selectedPage >= 2{
            if indexPath.row == 0 {
                cell.lbl_PageNumber.text = "<<Previous"
            }else {
                cell.lbl_PageNumber.text = "\(indexPath.row)"
            }
            cell.lbl_PageNumber.textColor = UIColor.init(hax: 0x333333, alpah: 1)
            cell.lbl_BottomBoarder.backgroundColor = .clear
            
            if selectedPage == indexPath.row {
                cell.lbl_PageNumber.textColor = AppColor.OrangeColor
                cell.lbl_BottomBoarder.backgroundColor = AppColor.OrangeColor
            }
            
            if totalPage == selectedPage + 1 && totalPage + 1 == indexPath.row + 1{
                cell.lbl_PageNumber.text = ""
            }
            else if totalPage + 1 == indexPath.row + 1
            {
                cell.lbl_PageNumber.text = "Next>>"
            }
        } else {
            cell.lbl_PageNumber.text = "\(indexPath.row + 1)"
            cell.lbl_PageNumber.textColor = UIColor.init(hax: 0x333333, alpah: 1)
            cell.lbl_BottomBoarder.backgroundColor = .clear
            if selectedPage == indexPath.row + 1 {
                cell.lbl_PageNumber.textColor = AppColor.OrangeColor
                cell.lbl_BottomBoarder.backgroundColor = AppColor.OrangeColor
            }
            if totalPage == indexPath.row + 1
            {
                cell.lbl_PageNumber.text = "Next>>"
            }
        }
        
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var width = 21
        if selectedPage >= 2{
            if indexPath.row == 0 {
                width = 80
            }
            if totalPage == selectedPage + 1 && totalPage + 1 == indexPath.row + 1{
                width = 0
            }
            else if totalPage + 1 == indexPath.row + 1
            {
                width = 50
            }
            
        } else {
            if totalPage == indexPath.row + 1
            {
                width = 50
            }
        }
        
         //some width
        let height = 30 //ratio
        return CGSize(width: width, height: height)
    }
    // tap on page number and next and previous buttons
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if selectedPage >= 2{
        
            if indexPath.row == 0 {
                // tap on previous
                selectedPage -= 1
            }
            else if totalPage + 1 == indexPath.row + 1 {
                //tap on next
                selectedPage += 1
            }
            else {
                //tap on page number
                selectedPage = indexPath.row
            }
            
        } else {
            if totalPage == indexPath.row + 1
            {// tap on next
                selectedPage += 1
            } else {
                //tap on page number
                selectedPage = indexPath.row + 1
            }
            
        }
        //call api with start value
        self.ApiGetStudentList(page: "\(selectedPage)")
        collectionView.reloadData()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) { // Change `2.0` to the desired number of seconds.
            self.collecationViewWidthConstraint.constant = CGFloat(self.getCollecationViewContentsize())
        }
    }
}
