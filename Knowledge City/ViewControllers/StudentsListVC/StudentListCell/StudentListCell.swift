//
//  StudentListCell.swift
//  Knowledge City
//
//  Created by Latitude on 09/10/19.
//  Copyright © 2019 Latitude. All rights reserved.
//

import UIKit

class StudentListCell: UITableViewCell {

    @IBOutlet weak var lbl_UserName: UILabel!
    @IBOutlet weak var lbl_FullName: UILabel!
    @IBOutlet weak var lbl_GroupName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
