//
//  LoginVC.swift
//  Knowledge City
//
//  Created by Latitude on 09/10/19.
//  Copyright © 2019 Latitude. All rights reserved.
//

import UIKit

class LoginVC: UIViewController {

    @IBOutlet weak var btn_Remember: UIButton!
    @IBOutlet weak var img_Password_ic: UIImageView!
    @IBOutlet weak var img_user_ic: UIImageView!
    @IBOutlet weak var view_PasswordText: K_UIView!
    @IBOutlet weak var view_UsernameText: K_UIView!
    @IBOutlet weak var txt_Username: UITextField!
    @IBOutlet weak var txt_Password: UITextField!
    var is_Remember = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        // get remember username and password
        if K_UserDefaults.GetUserName().count != 0 && K_UserDefaults.GetUserPassword().count != 0 {
            txt_Username.text = K_UserDefaults.GetUserName()
            txt_Password.text = K_UserDefaults.GetUserPassword()
            btn_Remember.setImage(UIImage.init(named: "radia_active"), for: .normal)
            is_Remember = true
        }
    }
    
    // on tap login button
    @IBAction func OntapLoginBTN(_ sender: Any) {
        // check validation
        if txt_Username.text?.count == 0 {
            CommonFunctions.shared.showAlertMessage(vc: self, titleStr: "", messageStr: "Please enter username.")
        }
        else if txt_Password.text?.count == 0 {
            CommonFunctions.shared.showAlertMessage(vc: self, titleStr: "", messageStr: "Please enter password")
        } else {
            
            // save remember username & passowrd
            if is_Remember == true {
                K_UserDefaults.SaveUserName(type: txt_Username.text!)
                K_UserDefaults.SaveUserPassword(type: txt_Password.text!)
            } else {
                K_UserDefaults.SaveUserName(type:"")
                K_UserDefaults.SaveUserPassword(type: "")
            }
            // login api calling
            ApiLogin()
        }
        
    }
    // button event remember password
    @IBAction func onTapRememberMeBTN(_ sender: UIButton) {
        
        if sender.currentImage == UIImage.init(named: "radia_active") {
            sender.setImage(UIImage.init(named: "radia_inactive"), for: .normal)
            is_Remember = false
        } else {
            sender.setImage(UIImage.init(named: "radia_active"), for: .normal)
            is_Remember = true
        }
    }
    //MARK: Login API
    
    func ApiLogin() {
        
        CommonFunctions.shared.ShowHUD()
        let param = ["username" : "\(txt_Username.text ?? "")",
            "password" : "\(txt_Password.text ?? "")",
                     "usertype" : "accountAdmin",
                     "_extend" : "user"] as [String:AnyObject]
        
        AFMethods.requestPOSTURL(Api_Login, params: param, headers: nil, success: { (response) in
            print(response)
            CommonFunctions.shared.HideHUD()
            let responsedata = ApiResponseStruct.init(json: response)
            
            if responsedata.code == 200 {
                let dic = responsedata.dataDictionary
                K_UserDefaults.SaveToken(type: dic["token"] as! String)
                K_UserDefaults.SaveAccount_id(type: dic["user"]!["account_id"] as! String)
                
                self.navigationController?.isNavigationBarHidden = false
                let vc = StudentsListVC.init(nibName: Nibs.StudentsListVC, bundle: nil)
                self.navigationController?.pushViewController(vc, animated: true)
            }else {
                
            }
            
        }) { (error) in
            print(error)
            CommonFunctions.shared.HideHUD()
        }
    }

}

extension LoginVC: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == txt_Username {
            view_UsernameText.layer.borderColor = AppColor.OrangeColor.cgColor
            view_PasswordText.layer.borderColor = AppColor.boarderColor.cgColor
            let imguser = img_user_ic.image
            img_user_ic.image = imguser!.maskWithColor(color:  AppColor.OrangeColor)
            let imgpass = img_Password_ic.image
            img_Password_ic.image = imgpass!.maskWithColor(color:  AppColor.boarderColor)
            
        } else {
            view_UsernameText.layer.borderColor = AppColor.boarderColor.cgColor
            view_PasswordText.layer.borderColor = AppColor.OrangeColor.cgColor
            let imguser = img_user_ic.image
            img_user_ic.image = imguser!.maskWithColor(color:  AppColor.boarderColor)
            let imgpass = img_Password_ic.image
            img_Password_ic.image = imgpass!.maskWithColor(color:  AppColor.OrangeColor)
        }
    }
}
extension UIImage {
    
    func maskWithColor(color: UIColor) -> UIImage? {
        let maskImage = cgImage!
        
        let width = size.width
        let height = size.height
        let bounds = CGRect(x: 0, y: 0, width: width, height: height)
        
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        let bitmapInfo = CGBitmapInfo(rawValue: CGImageAlphaInfo.premultipliedLast.rawValue)
        let context = CGContext(data: nil, width: Int(width), height: Int(height), bitsPerComponent: 8, bytesPerRow: 0, space: colorSpace, bitmapInfo: bitmapInfo.rawValue)!
        
        context.clip(to: bounds, mask: maskImage)
        context.setFillColor(color.cgColor)
        context.fill(bounds)
        
        if let cgImage = context.makeImage() {
            let coloredImage = UIImage(cgImage: cgImage)
            return coloredImage
        } else {
            return nil
        }
    }
    
}
