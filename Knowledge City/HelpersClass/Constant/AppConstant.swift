//
//  AppConstant.swift
//  Knowledge City
//
//  Created by Latitude on 09/10/19.
//  Copyright © 2019 Latitude. All rights reserved.
//

import Foundation
import UIKit

// App title
let Appname = ""


// Device size
let IS_IPHONE_X_Xs = fabs((Double(UIScreen.main.bounds.size.height) - Double(812.0))) < Double.ulpOfOne
let IS_IPHONE_Xs_Max = fabs((Double(UIScreen.main.bounds.size.height) - Double(896.0))) < Double.ulpOfOne
let IS_IPHONE_4 = fabs((Double(UIScreen.main.bounds.size.height) - Double(480))) < Double.ulpOfOne
let IS_IPHONE_5_5S_5C = fabs((Double(UIScreen.main.bounds.size.height) - Double(568))) < Double.ulpOfOne
let IS_IPHONE_6_6S_7_8 = fabs((Double(UIScreen.main.bounds.size.height) - Double(568.0))) < Double.ulpOfOne
let IS_IPHONE_6_6S_7_8_PLUS = fabs((Double(UIScreen.main.bounds.size.height) - Double(667.0))) < Double.ulpOfOne


// App delegate
let ApplicationDelegate = UIApplication.shared.delegate as! AppDelegate

//  Device Type
let DeviceType = "1" // ios = 1

// App Language
let AppLocalization = "en"


// MARK:- Colors
struct AppColor {
    static let OrangeColor = UIColor.init(hax: 0xe98c3b, alpah: 1) // Orange
    static let LiteGrayColor = UIColor.init(hax: 0xf6f6f6, alpah: 1) // backgroud color
    static let boarderColor = UIColor.init(hax: 0x808080, alpah: 1)
}


//MARK:- FontsName Struct
struct Fonts
{
    static let F_OpenSans_Bold = "OpenSans-Bold"
    static let F_OpenSans_BoldItalic = "OpenSans-BoldItalic"
    static let F_OpenSans_ExtraBold = "OpenSans-ExtraBold"
    static let F_OpenSans_ExtraBoldItalic = "OpenSans-ExtraBoldItalic"
    static let F_OpenSans_Italic = "OpenSans-Italic"
    static let F_OpenSans_Light = "OpenSans-Light"
    static let F_OpenSans_LightItalic = "OpenSans-LightItalic"
    static let F_OpenSans_Regular = "OpenSans-Regular"
    static let F_OpenSans_SemiBold = "OpenSans-SemiBold"
    static let F_OpenSans_SemiBoldItalic = "OpenSans_SemiBoldItalic"
    
    
}

// MARK:- All App Keys
struct Keys {
   
}
//MARK: Observer for notification center
struct NotificationObserver {
    // Observer notifications
    struct Name {
        
    }
    
}
//MARK: remote notification Set NT
struct RemoteNotification {
    // For remote notification
    struct Notification {
        
    }
}

// MARK:- ViewController Nib Names
struct Nibs {
    static let LoginVC = "LoginVC"
    static let StudentsListVC = "StudentsListVC"
}

//MARK: Cell Nib Names
struct CellNibs {
    
    // for tableview cell
    static let StudentListCell = "StudentListCell"
    
    // for collecationview Cell
    static let PageCell = "PageCell"
}




