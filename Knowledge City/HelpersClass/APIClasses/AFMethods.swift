//
//  AFMethods.swift
//  Knowledge City
//
//  Created by Latitude on 09/10/19.
//  Copyright © 2019 Latitude. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import SVProgressHUD

class AFMethods: Alamofire.SessionManager
{
    
    //MARK: cancel api calling
    class func CancelRequest(urlString: String){
        let sessionManager = self.default
        sessionManager.session.getTasksWithCompletionHandler { dataTasks, uploadTasks, downloadTasks in
            dataTasks.forEach {
                if ($0.originalRequest?.url?.absoluteString.contains(urlString))!
                {
                    $0.cancel()
                }
            }
            //uploadTasks.forEach { $0.cancel() }
            //downloadTasks.forEach { $0.cancel() }
        }
    }
    // MARK:- get method
    class func requestGETURL(_ strURL: String, params : [String : AnyObject]?, headers : [String : String]?, success:@escaping (JSON) -> Void, failure:@escaping (Error) -> Void) {
        
        self.default.request(strURL, method: .get, parameters: params, encoding: URLEncoding(destination: .queryString), headers: headers).responseJSON { (responseObject) in
            
            if responseObject.result.isSuccess {
                let resJson = JSON(responseObject.result.value!)
                success(resJson)
            }
            if responseObject.result.isFailure {
                let error : Error = responseObject.result.error!
                failure(error)
            }
        }
    }
    
    
    // MARK:- post method
    class func requestPOSTURL(_ strURL : String, params : [String : AnyObject]?, headers : [String : String]?, success:@escaping (JSON) -> Void, failure:@escaping (Error) -> Void){
        
        self.default.request(strURL, method: .post, parameters: params, encoding: URLEncoding(destination: .queryString), headers: headers).responseJSON { (responseObject) in
            
            if responseObject.result.isSuccess {
                let resJson = JSON(responseObject.result.value!)
                success(resJson)
            }
            if responseObject.result.isFailure {
                let error : Error = responseObject.result.error!
                failure(error)
            }
        }
        
    }
    
    // MARK:- Delete method
    class func requestDeleteURL(_ strURL: String, params : [String : AnyObject]?, headers : [String : String]?, success:@escaping (JSON) -> Void, failure:@escaping (Error) -> Void) {
        
        self.default.request(strURL, method: .delete, parameters: params, encoding: URLEncoding(destination: .queryString), headers: headers).responseJSON { (responseObject) in
            print(responseObject)
            
            if responseObject.result.isSuccess {
                let resJson = JSON(responseObject.result.value!)
                success(resJson)
            }
            if responseObject.result.isFailure {
                let error : Error = responseObject.result.error!
                failure(error)
            }
        }
    }
    
    // MARK:- upload singal image with param
    class func requestPOSTURLwithUplodeImage(_ strURL : String, params : [String: String]?,img_param_Name : String, image : UIImageView?, headers : [String : String]?, success:@escaping (JSON) -> Void, failure:@escaping (Error) -> Void){
        
        self.default.upload(multipartFormData:
            {
                multipartFormData in
                
                if image != nil
                {
                    if let imageData = (image?.image!)!.pngData()
                    {
                        multipartFormData.append(imageData, withName: img_param_Name, fileName: "image.png", mimeType: "image/png")
                    }
                }
                for (key, value) in params!
                {
                    multipartFormData.append((value.data(using: .utf8))!, withName: key)
                }
                
        }, to: strURL, method: .post, headers: headers,
           encodingCompletion: { encodingResult in
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    let resJson = JSON(response.result.value!)
                    success(resJson)
                }
            case .failure(let encodingError):
                let error : Error = encodingError
                failure(error)
                
            }
        })
    }
    
    // MARK:- upload multipal image with peram
    class func requestPOSTURLwithUplodeMultiepleImage(_ strURL : String, params : [String: String]?, image : Array<Any>, headers : [String : String]?, uploadProgress:@escaping (Progress) -> Void, success:@escaping (JSON) -> Void, failure:@escaping (Error) -> Void)
    {
        
        self.default.upload(multipartFormData:
            {
                multipartFormData in
                var count = 1
                for img in image
                {
                    let imgdata = (img as! UIImage).pngData()//UIImageJPEGRepresentation(img as! UIImage, 1.0)
                    multipartFormData.append(imgdata!,withName: "file[]", fileName: "img\(count).png", mimeType: "image/png")
                    count += 1
                }
                for (key, value) in params!
                {
                    multipartFormData.append((value.data(using: .utf8))!, withName: key)
                }
                
        }, to: strURL, method: .post, headers: headers,
           encodingCompletion: { encodingResult in
            switch encodingResult {
            case .success(let upload, _, _):
                upload.uploadProgress(closure: { (progress) in
                    //Print progress
                    //print("upload image:-  \(progress)")
                    uploadProgress(progress)
                })
                upload.responseJSON { response in
                    let resJson = JSON(response.result.value!)
                    success(resJson)
                }
            case .failure(let encodingError):
                let error : Error = encodingError
                failure(error)
                
            }
        })
    }
}

// Api Struct

struct ApiResponseStruct {
    
    var status = String()
    var code = NSInteger()
    var message = String()
    var dataDictionary = [String:AnyObject]()
    var Pagination = [String:AnyObject]()
    var dataArray = [[String:AnyObject]]()
    
    init(json:JSON){
        
        // code
        if let code = json["code"].int {
            self.code  = code
        }
        // status
        if let status = json["status"].string {
            self.status  = status
        }
        
        // api data deictionray formate
        if let d = json["response"].dictionaryObject {
            dataDictionary = d as [String : AnyObject]
        }
        
        // api data Pagination
        if let d = json["pagination"].dictionaryObject {
            Pagination = d as [String : AnyObject]
        }
        
        // api data in array formate
        if let arr = json["response"].arrayObject {
            dataArray = arr as! [[String : AnyObject]]
        }
        
    }
}
