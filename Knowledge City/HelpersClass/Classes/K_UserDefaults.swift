//
//  K_UserDefaults.swift
//  Knowledge City
//
//  Created by Latitude on 09/10/19.
//  Copyright © 2019 Latitude. All rights reserved.
//

import Foundation

let U_token = "token"
let U_account_id = "accountid"
let U_rememberusername = "rememberusername"
let U_rememberPass = "rememberPass"



class K_UserDefaults: NSObject
{
    /// save Token
    class func SaveToken(type:String)
    {
        UserDefaults.standard.set(type, forKey: U_token)
        UserDefaults.standard.synchronize()
    }
    // Get token
    class func GetToken() -> String
    {
        return ((UserDefaults.standard.value(forKey: U_token)) == nil) ? "":UserDefaults.standard.value(forKey: U_token) as! String
    }
    
    /// save U_account_id
    class func SaveAccount_id(type:String)
    {
        UserDefaults.standard.set(type, forKey: U_account_id)
        UserDefaults.standard.synchronize()
    }
    // Get U_account_id
    class func GetAccount_id() -> String
    {
        return ((UserDefaults.standard.value(forKey: U_account_id)) == nil) ? "":UserDefaults.standard.value(forKey: U_account_id) as! String
    }
    
    /// save UserName
    class func SaveUserName(type:String)
    {
        UserDefaults.standard.set(type, forKey: U_rememberusername)
        UserDefaults.standard.synchronize()
    }
    // Get UserName
    class func GetUserName() -> String
    {
        return ((UserDefaults.standard.value(forKey: U_rememberusername)) == nil) ? "":UserDefaults.standard.value(forKey: U_rememberusername) as! String
    }
    
    /// save UserPassword
    class func SaveUserPassword(type:String)
    {
        UserDefaults.standard.set(type, forKey: U_rememberPass)
        UserDefaults.standard.synchronize()
    }
    // Get UserPassword
    class func GetUserPassword() -> String
    {
        return ((UserDefaults.standard.value(forKey: U_rememberPass)) == nil) ? "":UserDefaults.standard.value(forKey: U_rememberPass) as! String
    }
}
