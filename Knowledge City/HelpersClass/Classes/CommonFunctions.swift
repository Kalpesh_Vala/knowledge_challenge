//
//  CommonFunctions.swift
//  Knowledge City
//
//  Created by Latitude on 09/10/19.
//  Copyright © 2019 Latitude. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage
import SVProgressHUD

class CommonFunctions {
    
    static var shared = CommonFunctions()
    
    // Email Valid or Not
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "^(?:(?:(?:(?: )*(?:(?:(?:\\t| )*\\r\\n)?(?:\\t| )+))+(?: )*)|(?: )+)?(?:(?:(?:[-A-Za-z0-9!#$%&’*+/=?^_'{|}~]+(?:\\.[-A-Za-z0-9!#$%&’*+/=?^_'{|}~]+)*)|(?:\"(?:(?:(?:(?: )*(?:(?:[!#-Z^-~]|\\[|\\])|(?:\\\\(?:\\t|[ -~]))))+(?: )*)|(?: )+)\"))(?:@)(?:(?:(?:[A-Za-z0-9](?:[-A-Za-z0-9]{0,61}[A-Za-z0-9])?)(?:\\.[A-Za-z0-9](?:[-A-Za-z0-9]{0,61}[A-Za-z0-9])?)*)|(?:\\[(?:(?:(?:(?:(?:[0-9]|(?:[1-9][0-9])|(?:1[0-9][0-9])|(?:2[0-4][0-9])|(?:25[0-5]))\\.){3}(?:[0-9]|(?:[1-9][0-9])|(?:1[0-9][0-9])|(?:2[0-4][0-9])|(?:25[0-5]))))|(?:(?:(?: )*[!-Z^-~])*(?: )*)|(?:[Vv][0-9A-Fa-f]+\\.[-A-Za-z0-9._~!$&'()*+,;=:]+))\\])))(?:(?:(?:(?: )*(?:(?:(?:\\t| )*\\r\\n)?(?:\\t| )+))+(?: )*)|(?: )+)?$"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: testStr)
        return result
    }
    // String To Uiimage 
    func getimageFromString(imgstr:String, imgview:UIImageView, placeholderImage: String, activityIndicatorStyle : UIActivityIndicatorView.Style) {
        
        let url = URL(string:imgstr)
        imgview.sd_imageIndicator?.indicatorView.isHidden = false
        imgview.sd_imageIndicator = SDWebImageActivityIndicator.gray
        imgview.sd_imageIndicator?.startAnimatingIndicator()
        imgview.sd_setImage(with: url, placeholderImage: (placeholderImage.count != 0) ? UIImage.init(named: placeholderImage):nil, options: .lowPriority) { (loadedImage, error, cacheType, url) in
            imgview.sd_imageIndicator?.indicatorView.isHidden = true
            imgview.sd_imageIndicator?.stopAnimatingIndicator()
            if error != nil {
                //print("Error code: \(error!.localizedDescription)")
            } else {
                imgview.image = loadedImage
            }

        }
    }
    
    // Remove null Object In Dictionary
    func Remove_Null_Object_From_DictionaryResponse(dic:NSDictionary) -> NSMutableDictionary {
        let allKeys = dic.allKeys
        let dicTrimNull = NSMutableDictionary()
        for i in 0...allKeys.count - 1 {
            let keyValue = dic[allKeys[i]]
            if keyValue is NSNull {
                dicTrimNull.setValue("", forKey: allKeys[i] as! String)
            }
            else {
                dicTrimNull.setValue(keyValue, forKey: allKeys[i] as! String)
            }
        }
        return dicTrimNull
    }
   
    // Remove Space in last object from String
    func trimmingTrailingSpaces(string : String) -> String {
        var t : String = string
        while t.hasSuffix(" ") {
            t = "" + t.dropLast()
        }
        return t
    }
   
    //MARK: show svprogressHUD
    func ShowHUD()
    {
        SVProgressHUD.setDefaultStyle(.custom)
        SVProgressHUD.setDefaultMaskType(.custom)
        SVProgressHUD.setForegroundColor(AppColor.OrangeColor)           //Ring Color
        //SVProgressHUD.setBackgroundColor(AppColor.charcoalDarkGray)        //HUD Color
        SVProgressHUD.setBackgroundLayerColor(UIColor.clear)    //Background Color
        SVProgressHUD.setRingThickness(5.0)
        SVProgressHUD.show()
    }
    
    func HideHUD()
    {
        SVProgressHUD.dismiss()
    }
    // textfield trimming white spaces characters
    func isTextfieldBlank (_ textField : UITextField) -> Bool
    {
        if (textField.text?.count)! > 0
        {
            let trimmedString = textField.text!.trimmingCharacters(in: CharacterSet.whitespaces)
            if trimmedString.count > 0
            {
                return false
            }
            return true
        }
        return true
    }
    
    // Show Alert
    func showAlertMessage(vc: UIViewController, titleStr:String, messageStr:String) -> Void {
        
        let alert = UIAlertController(title: titleStr, message: messageStr, preferredStyle: UIAlertController.Style.alert);
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        vc.present(alert, animated: true, completion: nil)
    }
    //MARK: set constraint on view
    func constrintEqulTo(HolderView: UIView, subview: UIView){
        subview.translatesAutoresizingMaskIntoConstraints = false
        let pinTop = NSLayoutConstraint(item: HolderView, attribute: .top, relatedBy: .equal,
                                        toItem: subview, attribute: .top, multiplier: 1.0, constant: 0)
        let pinHeight = NSLayoutConstraint(item: HolderView, attribute: .bottom, relatedBy: .equal,
                                           toItem: subview, attribute: .bottom, multiplier: 1.0, constant: 0)
        let pinLeft = NSLayoutConstraint(item: HolderView, attribute: .left, relatedBy: .equal,
                                         toItem: subview, attribute: .left, multiplier: 1.0, constant: 0)
        let pinRight = NSLayoutConstraint(item: HolderView, attribute: .right, relatedBy: .equal,
                                          toItem: subview, attribute: .right, multiplier: 1.0, constant: 0)
        
        HolderView.addConstraints([pinTop, pinHeight, pinLeft, pinRight])
    }
    
}


